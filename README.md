# Github Jobs

Github jobs search mobile application using github api.

Google play store: https://play.google.com/store/apps/details?id=com.phyohtet.githubjobs

# Screenshots

<img src="Screenshots/ios-githubjobs-list.png"  width="100" height="200">
<img src="Screenshots/ios-githubjob-detail.png"  width="100" height="200">
<img src="Screenshots/android-githubjobs-list.png"  width="100" height="200">
<img src="Screenshots/android-githubjobs-detail.png"  width="100" height="200">
<img src="Screenshots/android-githubjobs-filter.png"  width="100" height="200">